package entity

import "gitee.com/bigmouth/quick-mock/src/domain/value"

type MatchRule struct {
	MatchType value.MatchType // query param, json, url+param+json
	DataPath  string          // 待匹配的数据路径
	Pattern   string
}

func NewMatchRule(matchTypeStr, dataPath, pattern string) (MatchRule, error) {
	matchType, err := value.GetMatchType(matchTypeStr)
	if err != nil {
		return MatchRule{}, err
	}
	return MatchRule{
		MatchType: matchType,
		DataPath:  dataPath,
		Pattern:   pattern,
	}, nil
}
