package entity

type Namespace struct {
	Name string
}

func NewNamespace(namespace string) Namespace {
	return Namespace{
		namespace,
	}
}
