package entity

import (
	"gitee.com/bigmouth/quick-mock/src/domain/value"
	"testing"
)

/**
* @Author: huangyi
* @Date: 2022/7/6 23:31
 */

func TestMockApi_IsMatchPath(t *testing.T) {
	type fields struct {
		Delay                int64
		matchTypeAndRulesMap map[value.MatchType][]MatchRule
	}
	type args struct {
		path string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{name: "普通字符串完整匹配", fields: fields{
			Delay: 0,
			matchTypeAndRulesMap: map[value.MatchType][]MatchRule{
				value.MatchTypePath: []MatchRule{
					{
						MatchType: value.MatchTypePath,
						DataPath:  "",
						Pattern:   "/test",
					},
				},
			},
		}, args: args{
			path: "/test",
		}, want: true},
		{name: "正则匹配", fields: fields{
			Delay: 0,
			matchTypeAndRulesMap: map[value.MatchType][]MatchRule{
				value.MatchTypePath: []MatchRule{
					{
						MatchType: value.MatchTypePath,
						DataPath:  "",
						Pattern:   "/test/\\d+",
					},
				},
			},
		}, args: args{
			path: "/test/1",
		}, want: true},
		{name: "无法匹配", fields: fields{
			Delay: 0,
			matchTypeAndRulesMap: map[value.MatchType][]MatchRule{
				value.MatchTypePath: []MatchRule{
					{
						MatchType: value.MatchTypePath,
						DataPath:  "",
						Pattern:   "/test/\\d+",
					},
				},
			},
		}, args: args{
			path: "/test/a",
		}, want: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MockApi{
				Delay:                tt.fields.Delay,
				matchTypeAndRulesMap: tt.fields.matchTypeAndRulesMap,
			}
			if got, _ := m.IsMatchPath(tt.args.path); got != tt.want {
				t.Errorf("IsMatchPath() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMockApi_IsMatchQueryParam(t *testing.T) {
	type fields struct {
		matchTypeAndRulesMap map[value.MatchType][]MatchRule
	}
	type args struct {
		queryParam map[string]string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{name: "单匹配", fields: fields{
			matchTypeAndRulesMap: map[value.MatchType][]MatchRule{
				value.MatchTypeQueryParam: []MatchRule{
					{
						MatchType: value.MatchTypeQueryParam,
						DataPath:  "key1",
						Pattern:   "value1",
					},
				},
			},
		}, args: args{
			queryParam: map[string]string{
				"key1": "value1",
			},
		}, want: true},
		{name: "多条匹配", fields: fields{
			matchTypeAndRulesMap: map[value.MatchType][]MatchRule{
				value.MatchTypeQueryParam: []MatchRule{
					{
						MatchType: value.MatchTypeQueryParam,
						DataPath:  "key1",
						Pattern:   "value1",
					},
					{
						MatchType: value.MatchTypeQueryParam,
						DataPath:  "key2",
						Pattern:   "value2",
					},
				},
			},
		}, args: args{
			queryParam: map[string]string{
				"key1": "value1",
				"key2": "value2",
			},
		}, want: true},
		{name: "多条匹配-无法同时匹配", fields: fields{
			matchTypeAndRulesMap: map[value.MatchType][]MatchRule{
				value.MatchTypeQueryParam: []MatchRule{
					{
						MatchType: value.MatchTypeQueryParam,
						DataPath:  "key1",
						Pattern:   "value1",
					},
					{
						MatchType: value.MatchTypeQueryParam,
						DataPath:  "key2",
						Pattern:   "value2",
					},
				},
			},
		}, args: args{
			queryParam: map[string]string{
				"key1": "value1",
			},
		}, want: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MockApi{
				matchTypeAndRulesMap: tt.fields.matchTypeAndRulesMap,
			}
			if got := m.IsMatchQueryParam(tt.args.queryParam); got != tt.want {
				t.Errorf("IsMatchQueryParam() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMockApi_IsMatchMethod(t *testing.T) {
	type fields struct {
		matchTypeAndRulesMap map[value.MatchType][]MatchRule
	}
	type args struct {
		method string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		{name: "单个method", fields: fields{
			matchTypeAndRulesMap: map[value.MatchType][]MatchRule{
				value.MatchTypeMethod: []MatchRule{
					{
						MatchType: value.MatchTypeMethod,
						Pattern:   "post",
					},
				},
			},
		}, args: args{method: "post"}, want: true, wantErr: false},
		{name: "多个method", fields: fields{
			matchTypeAndRulesMap: map[value.MatchType][]MatchRule{
				value.MatchTypeMethod: []MatchRule{
					{
						MatchType: value.MatchTypeMethod,
						Pattern:   "post",
					},
					{
						MatchType: value.MatchTypeMethod,
						Pattern:   "get",
					},
				},
			},
		}, args: args{method: "post"}, want: true, wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MockApi{
				matchTypeAndRulesMap: tt.fields.matchTypeAndRulesMap,
			}
			got, err := m.IsMatchMethod(tt.args.method)
			if (err != nil) != tt.wantErr {
				t.Errorf("IsMatchMethod() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("IsMatchMethod() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMockApi_IsMatchBodyJsonPattern(t *testing.T) {
	type fields struct {
		matchTypeAndRulesMap map[value.MatchType][]MatchRule
	}
	type args struct {
		bodyJson map[string]interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		{name: "匹配单个path", fields: fields{
			matchTypeAndRulesMap: map[value.MatchType][]MatchRule{
				value.MatchTypeBodyJsonPattern: []MatchRule{
					{
						MatchType: value.MatchTypeBodyJsonPattern,
						DataPath:  "data.action_id",
						Pattern:   "item_tree",
					},
				},
			},
		}, args: args{
			bodyJson: map[string]interface{}{
				"game_id": 104,
				"data": map[string]interface{}{
					"action_id":         "item_tree",
					"logical_region_id": 1010,
					"module":            "common",
				},
			},
		}, want: true, wantErr: false},
		{name: "匹配多个path", fields: fields{
			matchTypeAndRulesMap: map[value.MatchType][]MatchRule{
				value.MatchTypeBodyJsonPattern: []MatchRule{
					{
						MatchType: value.MatchTypeBodyJsonPattern,
						DataPath:  "game_id",
						Pattern:   "104",
					}, {
						MatchType: value.MatchTypeBodyJsonPattern,
						DataPath:  "data.action_id",
						Pattern:   "item_tree",
					}, {
						MatchType: value.MatchTypeBodyJsonPattern,
						DataPath:  "data.module",
						Pattern:   "common",
					},
				},
			},
		}, args: args{
			bodyJson: map[string]interface{}{
				"game_id": 104,
				"data": map[string]interface{}{
					"action_id":         "item_tree",
					"logical_region_id": 1010,
					"module":            "common",
				},
			},
		}, want: true, wantErr: false},
		{name: "匹配多个path-没有全部匹配", fields: fields{
			matchTypeAndRulesMap: map[value.MatchType][]MatchRule{
				value.MatchTypeBodyJsonPattern: []MatchRule{
					{
						MatchType: value.MatchTypeBodyJsonPattern,
						DataPath:  "game_id",
						Pattern:   "108",
					}, {
						MatchType: value.MatchTypeBodyJsonPattern,
						DataPath:  "data.action_id",
						Pattern:   "item_tree",
					}, {
						MatchType: value.MatchTypeBodyJsonPattern,
						DataPath:  "data.module",
						Pattern:   "common",
					},
				},
			},
		}, args: args{
			bodyJson: map[string]interface{}{
				"game_id": 104,
				"data": map[string]interface{}{
					"action_id":         "item_tree",
					"logical_region_id": 1010,
					"module":            "common",
				},
			},
		}, want: false, wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MockApi{
				matchTypeAndRulesMap: tt.fields.matchTypeAndRulesMap,
			}
			got, err := m.IsMatchBodyJsonPattern(tt.args.bodyJson)
			if (err != nil) != tt.wantErr {
				t.Errorf("IsMatchBodyJsonPattern() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("IsMatchBodyJsonPattern() got = %v, want %v", got, tt.want)
			}
		})
	}
}
