package value

import "fmt"

/**
* @Author: huangyi
* @Date: 2022/7/6 22:49
 */

type MatchType string

const (
	MatchTypePath            MatchType = "path"              // 根据路径匹配
	MatchTypeMethod          MatchType = "method"            // 根据方法匹配
	MatchTypeHeader          MatchType = "header"            // 根据header
	MatchTypeQueryParam      MatchType = "query_param"       // 根据请求参数匹配
	MatchTypeBodyJsonPattern MatchType = "body_json_pattern" // 根据body的json路径匹配模式
)

func GetMatchType(matchType string) (MatchType, error) {
	switch matchType {
	case string(MatchTypePath):
		return MatchTypePath, nil
	case string(MatchTypeQueryParam):
		return MatchTypeQueryParam, nil
	case string(MatchTypeBodyJsonPattern):
		return MatchTypeBodyJsonPattern, nil
	case string(MatchTypeMethod):
		return MatchTypeMethod, nil
	case string(MatchTypeHeader):
		return MatchTypeHeader, nil
	default:
		return "", fmt.Errorf("不支持的匹配类型：%s", matchType)
	}
}
