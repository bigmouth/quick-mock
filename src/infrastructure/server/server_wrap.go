package server

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type Server struct {
	*gin.Engine
}

func (s *Server) GET(relativePath string, handler Handler) gin.IRoutes {
	return s.Engine.GET(relativePath, func(c *gin.Context) {
		respData, err := handler(WrapGinContext(c))
		if err != nil {
			c.JSON(http.StatusOK, Fail(err.Error()))
			return
		}
		c.JSON(http.StatusOK, SuccessWithData(respData))
	})
}

func (s *Server) handlerProxy(handler Handler) gin.HandlerFunc {
	return func(c *gin.Context) {
		respData, err := handler(WrapGinContext(c))
		if err != nil {
			c.JSON(http.StatusOK, Fail(err.Error()))
			return
		}
		c.JSON(http.StatusOK, SuccessWithData(respData))
	}
}

func (s *Server) POST(relativePath string, handler Handler) gin.IRoutes {
	return s.Engine.POST(relativePath, s.handlerProxy(handler))
}

func Default() *Server {

	return &Server{
		gin.Default(),
	}
}
