package server

import "strings"

const (
	RespCodeSuccess RespCode = 0
	RespCodeFail    RespCode = 1
)

type RespCode int

type Resp struct {
	Code RespCode    `json:"code"`
	Info string      `json:"info"`
	Data interface{} `json:"data,omitempty"`
}

func Success(msg ...string) Resp {
	if len(msg) > 1 {
		return Resp{
			Code: RespCodeSuccess,
			Info: strings.Join(msg, ","),
		}
	} else if len(msg) == 1 {
		return Resp{
			Code: RespCodeSuccess,
			Info: msg[0],
		}
	}
	return Resp{
		Code: RespCodeSuccess,
	}
}

func SuccessWithData(data JsonObject) Resp {
	return Resp{
		Code: RespCodeSuccess,
		Info: "",
		Data: data,
	}
}

func FailWithCode(code RespCode, msg ...string) Resp {
	if len(msg) > 1 {
		return Resp{
			Code: code,
			Info: strings.Join(msg, ","),
		}
	} else if len(msg) == 1 {
		return Resp{
			Code: code,
			Info: msg[0],
		}
	}
	return Resp{
		Code: code,
		Info: "unknown error",
	}
}

func Fail(msg ...string) Resp {
	return FailWithCode(RespCodeFail, msg...)
}
