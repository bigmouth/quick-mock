package server

type JsonObject interface{}

type Handler func(c *Context) (JsonObject, error)
