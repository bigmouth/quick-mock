package server

import "github.com/gin-gonic/gin"

type Context struct {
	*gin.Context
}

func WrapGinContext(c *gin.Context) *Context {
	return &Context{c}
}
