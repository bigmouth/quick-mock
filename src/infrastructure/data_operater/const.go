package data_operater

type TokenType int
type State int

const (
	Field        TokenType = 1
	OpArray      TokenType = 2
	OpProp       TokenType = 3
	OpMergeField TokenType = 4
	StateField   State     = 2
	StateOpStart State     = 3
	StateOpEnd   State     = 4
	StateStart   State     = 1
	StateEnd     State     = 4
	Num          string    = "0123456789"
	Chars        string    = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWZYX-_"
)
