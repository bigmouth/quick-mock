package data_operater

import (
	"reflect"
	"testing"
)

func TestGetData(t *testing.T) {

	type BaseInfo struct {
		Personal struct {
			Name string
		}
	}

	type args struct {
		path string
		data interface{}
	}
	tests := []struct {
		name    string
		args    args
		want    interface{}
		wantErr bool
	}{
		{name: "获取对象值", args: args{
			path: "Personal.Name",
			data: BaseInfo{
				Personal: struct{ Name string }{Name: "AAA"}},
		}, want: "AAA", wantErr: false},
		{name: "获取map对象值", args: args{
			path: "base_info.personal.name",
			data: map[string]interface{}{
				"base_info": map[string]interface{}{
					"personal": map[string]interface{}{
						"name": "BBB",
					},
				},
			},
		}, want: "BBB", wantErr: false},
		{name: "混搭map和struct", args: args{
			path: "base_info.Personal.Name",
			data: map[string]interface{}{
				"base_info": struct {
					Personal struct {
						Name string
					}
				}{
					struct {
						Name string
					}{
						Name: "AAA",
					},
				},
			},
		}, want: "AAA", wantErr: false},
		{name: "获取指定数组下标值的属性", args: args{
			path: "base_info.personal[1].name",
			data: map[string]interface{}{
				"base_info": map[string]interface{}{
					"personal": []map[string]interface{}{
						{"name": "BBB"},
						{"name": "AAA"},
					},
				},
			},
		}, want: "AAA", wantErr: false},
		{
			name: "获取指定数组下标值",
			args: args{
				path: "base_info.personal[0]",
				data: map[string]interface{}{
					"base_info": map[string]interface{}{
						"personal": []string{"1", "2"},
					},
				},
			},
			want:    "1",
			wantErr: false,
		},
		{name: "对象内纯数组值", args: args{
			path: "base_info.names[1]",
			data: map[string]interface{}{
				"base_info": map[string]interface{}{
					"names": []string{
						"AAA", "BBB",
					},
				},
			},
		}, want: "BBB", wantErr: false},
		{name: "直接获取数组-无数组标记", args: args{
			path: "base_info.names",
			data: map[string]interface{}{
				"base_info": map[string]interface{}{
					"names": []string{
						"AAA", "BBB",
					},
				},
			},
		}, want: []string{
			"AAA", "BBB",
		}, wantErr: false},
		{name: "直接获取数组-有数组标记", args: args{
			path: "base_info.names[]",
			data: map[string]interface{}{
				"base_info": map[string]interface{}{
					"names": []string{
						"AAA", "BBB",
					},
				},
			},
		}, want: []interface{}{
			"AAA", "BBB",
		}, wantErr: false},
		{name: "提取多个值，合成map", args: args{
			path: "base_info.personal.name|age",
			data: map[string]interface{}{
				"base_info": map[string]interface{}{
					"personal": map[string]interface{}{
						"name": "abc",
						"age":  10,
						"crop": "dc",
					},
				},
			},
		}, want: map[string]interface{}{
			"name": "abc",
			"age":  10,
		}, wantErr: false},
		{name: "提取多个值，合成map2", args: args{
			path: "base_info.personal.name|age|skill",
			data: map[string]interface{}{
				"base_info": map[string]interface{}{
					"personal": map[string]interface{}{
						"name":  "abc",
						"age":   10,
						"crop":  "dc",
						"skill": "绝对防守",
					},
				},
			},
		}, want: map[string]interface{}{
			"name":  "abc",
			"age":   10,
			"skill": "绝对防守",
		}, wantErr: false},
		{
			name: "空path",
			args: args{
				path: "",
				data: BaseInfo{
					Personal: struct{ Name string }{Name: "AAA"},
				},
			},
			want: BaseInfo{
				Personal: struct{ Name string }{Name: "AAA"},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetData(tt.args.path, tt.args.data)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetData() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetData() got = %#v, want %#v", got, tt.want)
			}
		})
	}
}
