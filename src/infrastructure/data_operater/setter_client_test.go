package data_operater

import (
	"context"
	assert2 "github.com/stretchr/testify/assert"
	"strconv"
	"testing"
)

func TestSetData(t *testing.T) {
	type args struct {
		obj      interface{}
		path     string
		provider SetterDataProvider
	}
	tests := []struct {
		name     string
		args     args
		wantErr  bool
		wantData interface{}
	}{
		{
			name: "正常传递-map",
			args: args{
				obj: map[string]interface{}{
					"personal": map[string]interface{}{
						"info": map[string]interface{}{
							"age": 10,
						},
					},
				},
				path: "personal.info",
				provider: func(ctx context.Context, original interface{}, fieldData interface{}) (map[string]interface{}, error) {
					return map[string]interface{}{
						"name": "aaa",
					}, nil
				},
			},
			wantErr: false,
			wantData: map[string]interface{}{
				"personal": map[string]interface{}{
					"info": map[string]interface{}{
						"age":  10,
						"name": "aaa",
					},
				},
			},
		},
		{
			name: "正常传递-array",
			args: args{
				obj: map[string]interface{}{
					"personal": map[string]interface{}{
						"info": []map[string]interface{}{
							{"age": 10},
							{"age": 11},
						},
					},
				},
				path: "personal.info[]",
				provider: func(ctx context.Context, original interface{}, fieldData interface{}) (map[string]interface{}, error) {
					return map[string]interface{}{
						"name": "aaa",
					}, nil
				},
			},
			wantErr: false,
			wantData: map[string]interface{}{
				"personal": map[string]interface{}{
					"info": []map[string]interface{}{
						{"age": 10, "name": "aaa"},
						{"age": 11, "name": "aaa"},
					},
				},
			},
		},
		{
			name: "正常传递-array-每行结果填充不同值",
			args: args{
				obj: map[string]interface{}{
					"personal": map[string]interface{}{
						"info": []map[string]interface{}{
							{"age": 10},
							{"age": 11},
						},
					},
				},
				path: "personal.info[]",
				provider: func(ctx context.Context, original interface{}, fieldData interface{}) (map[string]interface{}, error) {
					return map[string]interface{}{
						"name": strconv.Itoa(fieldData.(map[string]interface{})["age"].(int) + 1),
					}, nil
				},
			},
			wantErr: false,
			wantData: map[string]interface{}{
				"personal": map[string]interface{}{
					"info": []map[string]interface{}{
						{"age": 10, "name": "11"},
						{"age": 11, "name": "12"},
					},
				},
			},
		},
		{
			name: "正常传递-多维数组",
			args: args{
				obj: map[string]interface{}{
					"personal": map[string]interface{}{
						"info": [][]map[string]interface{}{
							[]map[string]interface{}{
								{"age": 10},
								{"age": 11},
							},
							[]map[string]interface{}{
								{"age": 20},
								{"age": 21},
							},
						},
					},
				},
				path: "personal.info[][]",
				provider: func(ctx context.Context, original interface{}, fieldData interface{}) (map[string]interface{}, error) {
					return map[string]interface{}{
						"name": strconv.Itoa(fieldData.(map[string]interface{})["age"].(int) + 1),
					}, nil
				},
			},
			wantErr: false,
			wantData: map[string]interface{}{
				"personal": map[string]interface{}{
					"info": [][]map[string]interface{}{
						{
							{"age": 10, "name": "11"},
							{"age": 11, "name": "12"},
						},
						{
							{"age": 20, "name": "21"},
							{"age": 21, "name": "22"},
						},
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := SetData(context.Background(), tt.args.obj, tt.args.path, tt.args.provider); (err != nil) != tt.wantErr {
				t.Errorf("SetData() error = %v, wantErr %v", err, tt.wantErr)
			}
			if !assert2.EqualValues(t, tt.wantData, tt.args.obj) {
				t.Errorf("SetData() wanted= %v, got= %v", tt.wantData, tt.args.obj)
			}
		})
	}
}
