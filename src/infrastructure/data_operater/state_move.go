package data_operater

import (
	"strings"
	"unicode"
)

// Start2FieldMove 开始->字段
type Start2FieldMove struct{}

func (s Start2FieldMove) GetSourceState() State {
	return StateStart
}

func (s Start2FieldMove) GetTargetState() State {
	return StateField
}

func (s Start2FieldMove) IsMatch(char string) bool {
	return strings.Index(Num, char) > -1 || strings.Index(Chars, char) > -1
}

// Op2OpMove 操作符->操作符
type Op2OpMove struct{}

func (s Op2OpMove) GetSourceState() State {
	return StateOpStart
}

func (s Op2OpMove) GetTargetState() State {
	return StateOpStart
}

func (s Op2OpMove) IsMatch(char string) bool {
	return strings.Index(Num, char) > -1
}

// OpEnd2FieldMove 操作符->字段
type OpEnd2FieldMove struct{}

func (s OpEnd2FieldMove) GetSourceState() State {
	return StateOpEnd
}

func (s OpEnd2FieldMove) GetTargetState() State {
	return StateField
}

func (s OpEnd2FieldMove) IsMatch(char string) bool {
	return strings.Index(Chars, char) > -1
}

// OpEnd2OpStartMove 操作符->字段
type OpEnd2OpStartMove struct{}

func (s OpEnd2OpStartMove) GetSourceState() State {
	return StateOpEnd
}

func (s OpEnd2OpStartMove) GetTargetState() State {
	return StateOpStart
}

func (s OpEnd2OpStartMove) IsMatch(char string) bool {
	return char == "." || char == "[" || char == "|"
}

// Field2FieldMove 字段->字段
type Field2FieldMove struct{}

func (s Field2FieldMove) GetSourceState() State {
	return StateField
}

func (s Field2FieldMove) GetTargetState() State {
	return StateField
}

func (s Field2FieldMove) IsMatch(char string) bool {
	runeData := []rune(char)
	return strings.Index(Num, char) > -1 || strings.Index(Chars, char) > -1 || unicode.Is(unicode.Han, runeData[0])
}

// Field2OpStartMove 字段->操作符
type Field2OpStartMove struct{}

func (s Field2OpStartMove) GetSourceState() State {
	return StateField
}

func (s Field2OpStartMove) GetTargetState() State {
	return StateOpStart
}

func (s Field2OpStartMove) IsMatch(char string) bool {
	return char == "." || char == "[" || char == "|"
}

// OpStart2OpEndMove 操作符开始->操作符结束
type OpStart2OpEndMove struct{}

func (s OpStart2OpEndMove) GetSourceState() State {
	return StateOpStart
}

func (s OpStart2OpEndMove) GetTargetState() State {
	return StateOpEnd
}

func (s OpStart2OpEndMove) IsMatch(char string) bool {
	return char == "]"
}

// OpStart2FieldMove 操作符开始->字段 （单操作符.)
type OpStart2FieldMove struct{}

func (s OpStart2FieldMove) GetSourceState() State {
	return StateOpStart
}

func (s OpStart2FieldMove) GetTargetState() State {
	return StateField
}

func (s OpStart2FieldMove) IsMatch(char string) bool {
	runeData := []rune(char)
	return strings.Index(Chars, char) > -1 || unicode.Is(unicode.Han, runeData[0])
}
