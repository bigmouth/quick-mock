package log

/**
* @Author: huangyi
* @Date: 2022/8/1 23:18
 */

type Level int

func (l *Level) GetName() string {
	switch *l {
	case DEBUG:
		return "DEBUG"
	case INFO:
		return "INFO"
	case WARN:
		return "WARN"
	case ERROR:
		return "ERROR"
	default:
		return "UNKNOWN"
	}
}

const (
	DEBUG = 1
	INFO  = 2
	WARN  = 3
	ERROR = 4
)
