package config

import "sync"

var configInstance *Config
var once = sync.Once{}

type Config struct {
	Mock Mock
}

func New(extTraceId string) {
	once.Do(func() {
		configInstance = &Config{
			Mock: Mock{
				ExtTraceIds: extTraceId,
			},
		}
	})
}

func GetConfig() *Config {
	if configInstance == nil {
		panic("config is nil")
	}
	return configInstance
}
