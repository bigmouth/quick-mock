package config

import "strings"

type Mock struct {
	ExtTraceIds string
}

func (m *Mock) GetExtTraceIds() []string {
	if m.ExtTraceIds == "" {
		return []string{}
	}
	extTraceIds := strings.ReplaceAll(m.ExtTraceIds, "，", ",")
	return strings.Split(extTraceIds, ",")
}
