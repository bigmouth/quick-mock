package base

import (
	"gitee.com/bigmouth/quick-mock/src/infrastructure/server"
)

func LoadRoute(s *server.Server) {
	s.GET("/health", health)
}
