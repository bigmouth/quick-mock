package base

import (
	"gitee.com/bigmouth/quick-mock/src/infrastructure/server"
)

func health(c *server.Context) (server.JsonObject, error) {
	return nil, nil
}
