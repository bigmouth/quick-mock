package web

import "gitee.com/bigmouth/quick-mock/src/infrastructure/server"

func LoadRoute(s *server.Server) {
	s.Engine.GET("/web", namespaceList)
	s.Engine.GET("/web/index", namespaceList)
	s.Engine.GET("/web/namespace/:namespace", mockApiList)

}
