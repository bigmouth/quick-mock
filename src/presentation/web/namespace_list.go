package web

import (
	"gitee.com/bigmouth/quick-mock/src/domain/service"
	"github.com/gin-gonic/gin"
	"net/http"
)

// 创建namespace
func namespaceList(c *gin.Context) {

	result := service.GetNamespaceInstance().GetNamespaceAndDisableTimeMap()
	c.HTML(http.StatusOK, "namespaces.html", gin.H{
		"namespaces": result,
	})
}
