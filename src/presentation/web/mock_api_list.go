package web

import (
	"gitee.com/bigmouth/quick-mock/src/domain/entity"
	"gitee.com/bigmouth/quick-mock/src/domain/service"
	"gitee.com/bigmouth/quick-mock/src/domain/value"
	"github.com/gin-gonic/gin"
	"net/http"
)

type MockApiRespData struct {
	Path             []entity.MatchRule
	Method           []entity.MatchRule
	Headers          []entity.MatchRule
	QueryParam       []entity.MatchRule
	BodyJsonPattern  []entity.MatchRule
	ResponseJsonData string
}

// mockApiList
func mockApiList(c *gin.Context) {
	namespace := c.Param("namespace")
	apiList := make([]MockApiRespData, 0)
	disableTimeStr := ""
	if agg, ok := service.GetNamespaceInstance().GetMocksByNamespace(namespace); ok {
		for _, api := range agg.GetMockApiList() {
			apiList = append(apiList, MockApiRespData{
				Path:             api.GetMatchRules(value.MatchTypePath),
				Method:           api.GetMatchRules(value.MatchTypeMethod),
				Headers:          api.GetMatchRules(value.MatchTypeHeader),
				QueryParam:       api.GetMatchRules(value.MatchTypeQueryParam),
				BodyJsonPattern:  api.GetMatchRules(value.MatchTypeBodyJsonPattern),
				ResponseJsonData: api.ResponseJsonDataStr,
			})
		}
		disableTimeStr = agg.GetDisableTimeStr()
	}
	c.HTML(http.StatusOK, "mock_api_list.html", gin.H{
		"namespace":   namespace,
		"apiList":     apiList,
		"disableTime": disableTimeStr,
	})

}
