package namespace

import "gitee.com/bigmouth/quick-mock/src/infrastructure/server"

func LoadRoute(s *server.Server) {
	s.POST("/namespace/create", create)
	s.POST("/namespace/addApi", addApi)
}
