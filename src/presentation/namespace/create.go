package namespace

import (
	"gitee.com/bigmouth/quick-mock/src/domain/service"
	"gitee.com/bigmouth/quick-mock/src/infrastructure/server"
)

type CreateReq struct {
	LifeTime int64 `json:"life_time"` // namespace 存在时间
}

type CreateResp struct {
	Namespace string `json:"namespace"`
}

// 创建namespace
func create(c *server.Context) (server.JsonObject, error) {
	req := CreateReq{
		LifeTime: 1000 * 60 * 3, // 生存时间3分钟
	}
	if err := c.BindJSON(&req); err != nil {
		return nil, err
	}
	namespace := service.GetNamespaceInstance().Create(req.LifeTime)
	return CreateResp{Namespace: namespace}, nil
}
