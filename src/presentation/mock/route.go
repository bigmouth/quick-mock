package mock

import "gitee.com/bigmouth/quick-mock/src/infrastructure/server"

/**
* @Author: huangyi
* @Date: 2022/7/7 00:36
 */

func LoadRoute(s *server.Server) {

	// 加载额外的traceId
	loadExtTraceIdKeys()

	s.Engine.GET("/mock/:namespace/*mockPath", Mock)
	s.Engine.POST("/mock/:namespace/*mockPath", Mock)
	s.Engine.HEAD("/mock/:namespace/*mockPath", Mock)
	s.Engine.PUT("/mock/:namespace/*mockPath", Mock)
	s.Engine.DELETE("/mock/:namespace/*mockPath", Mock)
	s.Engine.OPTIONS("/mock/:namespace/*mockPath", Mock)
	// 根据header的traceId作为namespace
	s.Engine.GET("/mockByTraceId/*mockPath", MockByTraceId)
	s.Engine.POST("/mockByTraceId/*mockPath", MockByTraceId)
	s.Engine.HEAD("/mockByTraceId/*mockPath", MockByTraceId)
	s.Engine.PUT("/mockByTraceId/*mockPath", MockByTraceId)
	s.Engine.DELETE("/mockByTraceId/*mockPath", MockByTraceId)
	s.Engine.OPTIONS("/mockByTraceId/*mockPath", MockByTraceId)
}
