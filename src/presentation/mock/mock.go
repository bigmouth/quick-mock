package mock

import (
	"github.com/gin-gonic/gin"
)

/**
* @Author: huangyi
* @Date: 2022/7/7 00:36
 */

func Mock(c *gin.Context) {
	// namespace
	namespace := c.Param("namespace")
	mockByNamespace(c, namespace)

}
