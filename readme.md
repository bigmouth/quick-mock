# quick-mock

[TOC]

## 简介

基于内存的mock服务，提供命名空间（namespace）用于隔离mock接口

## 安装
```bash
go install gitee.com/bigmouth/quick-mock@latest
```

## 启动

```bash
./main -port 9999

```

默认使用9999端口，可使用 -port 指定启动端口

### 启动参数
| 字段           | 默认值   | 说明                                  |
|--------------|-------|-------------------------------------|
| port         | 9999  | 启动端口                                |
| ext-trace-id | 空     | 扩展提取namespace的traceId的key，多个值时用逗号隔开 |
----

## 接口

### 基本响应格式

| 字段   | 类型     | 说明            |
|------|--------|---------------|
| code | int    | 状态，成功=0，失败>0  |
| info | string | 返回信息          |
| data | json   | 数据(无数据不返回该字段) |

----

### 服务健康检查

**路径:** /health

**方法:** get

**说明:** 无业务操作，直接返回成功，判断服务是否正常启动

**请求参数:**

无

**响应参数:**

无

**示例:**

请求

```
curl --location --request GET 'http://127.0.0.1:9999/health'
```

响应

```json
{
  "code": 0,
  "info": ""
}
```
---

### 创建命名空间

**路径:** /namespace/create

**方法:** post

**说明:**

创建临时的命名空间，用于隔离mock接口。

由于是数据存于内存，命名空间有存活时长，超时自动移除

**请求参数:**

| 字段        | 类型     | 说明                             |
|-----------|--------|--------------------------------|
| life_time | int | 命名空间存活时长，单位:毫秒(ms)。不传默认3分钟 |

**响应参数:**

| 字段        | 类型     | 说明            |
|-----------|--------|---------------|
| namespace | string | 命名空间唯一标识      |

**示例:**

请求

```json
{
  "life_time": 100000
}

```

响应

```json
{
  "code": 0,
  "info": "",
  "data": {
    "namespace": "be71fcec-3a27-4028-9fca-01a326cad759"
  }
}
```
---

### 添加mock接口

**路径:** /namespace/addApi

**方法:** post

**说明:**

为命名空间添加mock接口

**请求参数:**

| 字段        | 类型     | 说明                                                                            |
|-----------|--------|-------------------------------------------------------------------------------|
| namespace | string | 命名空间唯一标识                                                                      |
| delay          | int    | 响应延迟时间，单位毫秒(ms)                                                               |
| response_data| string | 响应json字符串, 支持go template语法，内置提供 {{.Body}}, {{.Header}}, {{.Param}}(url参数）三种对象 |
| match_rules| []matchRule| mock匹配规则                                                                      |

matchRule结构

| 字段        | 类型     | 说明                        |
|-----------|--------|---------------------------|
| match_type | string | 规则类型                      |
| data_path          | string | 匹配路径，根据matchType不同，有不同的作用 |
| pattern| string | 匹配值                       |

matchType枚举值：

|  类型     | 说明    |
|--------|-------|
| path| url路径 |
| method |请求方法|
| query_param |url参数|
|header |请求头参数|
|body_json_pattern| 请求体json参数|

详细案例参考:[matchType对应的格式说明](#matchType对应的格式说明)

**响应参数:**

| 字段        | 类型     | 说明            |
|-----------|--------|---------------|
| namespace | string | 命名空间唯一标识      |

**示例:**

请求

```json
{
    "namespace":"f60970e2-ccd4-43a8-8ea0-15c63ab85e51",
    "match_rules":[{
        "match_type":"path",
        "data_path": "",
        "pattern": "/test"
    },{
        "match_type":"method",
        "data_path": "",
        "pattern": "post"
    },{
        "match_type":"body_json_pattern",
        "data_path": "data.action",
        "pattern": "item_tree"
    },{
        "match_type":"body_json_pattern",
        "data_path": "data.module",
        "pattern": "common"
    }],
    "delay": 0,
    "response_data":"{\"code\":0,\"info\":\"lalalala\", \"action\":\"{{$.Body.data.action}}\"}"
}
```

响应

```json
{
  "code": 0,
  "info": ""
}
```

---

### 使用mock接口

**路径:** /mock/:namespace/*

**方法:** post/get/put/delete/head/options

**说明:**

使用mock接口，:namespace处传入使用create接口获取的namespace，后面 * 根据mock的path填写

**请求参数:**

你mock接口传入需要传入的参数

**响应参数:**

根据添加的mock中的responseData反序列后得到的json结构

**示例:**

创建的mock规则：
```json
{
    "namespace":"f60970e2-ccd4-43a8-8ea0-15c63ab85e51",
    "match_rules":[{
        "match_type":"path",
        "pattern": "/test1/test2"
    },{
        "match_type":"method",
        "pattern": "post"
    },{
        "match_type":"body_json_pattern",
        "data_path": "data.action",
        "pattern": "item_tree"
    },{
        "match_type":"body_json_pattern",
        "data_path": "data.module",
        "pattern": "common"
    }],
    "delay": 0,
    "response_data":"{\"code\":0,\"info\":\"lalalala\", \"action\":\"{{$.Body.data.action}}\"}"
}

```
请求
post 请求： 

/mock/f60970e2-ccd4-43a8-8ea0-15c63ab85e51/test1/test2

body
```json

{
  "data":{
    "module":"common",
    "action":"item_tree"
  }
}

```

响应

```json
{
  "action": "item_tree", // 这个字段值取自请求里的data.action
  "code": 0,
  "info": "lalalala"
}
```


---

### 根据trace-id使用mock接口

**路径:** /mockByTraceId/*

**方法:** post/get/put/delete/head/options

**说明:**

当不方便使用/mock 接口时，可以通过header中trace-id传递namespace

header中的trace-id支持以下几种中写法：
* trace_id
* trace-id
* Trace-Id
* Trace_Id
* TraceId
* Trace-ID
* Trace_ID

**请求参数:**

你mock接口传入需要传入的参数

**响应参数:**

根据添加的mock中的responseData反序列后得到的json结构

**示例:**

创建的mock规则：
```json
{
    "namespace":"f60970e2-ccd4-43a8-8ea0-15c63ab85e51",
    "match_rules":[{
        "match_type":"path",
        "pattern": "/test1/test2"
    },{
        "match_type":"method",
        "pattern": "post"
    },{
        "match_type":"body_json_pattern",
        "data_path": "data.action",
        "pattern": "item_tree"
    },{
        "match_type":"body_json_pattern",
        "data_path": "data.module",
        "pattern": "common"
    }],
    "delay": 0,
    "response_data":"{\"code\":0,\"info\":\"lalalala\"}"
}

```
请求
post 请求：

/mockByTraceId/test1/test2

header
```
trace-id=f60970e2-ccd4-43a8-8ea0-15c63ab85e51

```
body
```json

{
  "data":{
    "module":"common",
    "action":"item_tree"
  }
}

```

响应

```json
{
  "code": 0,
  "info": "lalalala"
}
```

## UI
适用版本号：>=v0.3.0

提供简单的web页面查看添加的规则
```html
http://localhost:9999/web

```

## 附加说明

### matchType对应的格式说明

#### path

匹配url路径，不包含url参数，只能配置一条记录

| 字段        | 类型     | 说明                         |
|-----------|--------|----------------------------|
| match_type | string | path                       |
| data_path          | string | 无用，可不传                   |
| pattern| string | 匹配值，支持正则。优先判断字符串是否全等，再使用正则 |

案例：

```json
{
  "match_type": "path",
  "data_path": "",
  "pattern": "/test"
}
```

#### method

匹配method，至少配置一条记录，多条规则之间是或关系

| 字段        | 类型     | 说明                         |
|-----------|--------|----------------------------|
| match_type | string | method                     |
| data_path          | string | 无用，可不传                   |
| pattern| string | 匹配值，支持正则。优先判断字符串是否全等，再使用正则 |

案例：

```json
{
  "match_type": "method",
  "data_path": "",
  "pattern": "post"
}
```

#### header

匹配header，多个header规则之间是且的关系

| 字段        | 类型     | 说明                         |
|-----------|--------|----------------------------|
| match_type | string | header                     |
| data_path          | string | header key                   |
| pattern| string | header value，不支持正则 |

案例：

```json
{
  "match_type": "header",
  "data_path": "key1",
  "pattern": "value1"
}
```

#### query_param

匹配url参数，多个规则之间是且的关系

| 字段        | 类型     | 说明                         |
|-----------|--------|----------------------------|
| match_type | string | query_param                     |
| data_path          | string | 参数 key                   |
| pattern| string | 参数 value，优先判断字符串是否全等，再使用正则 |

案例：

```json
{
  "match_type": "query_param",
  "data_path": "key1",
  "pattern": "value1"
}
```

#### body_json_pattern

匹配post请求中json key，多个规则之间是且的关系

| 字段        | 类型     | 说明                         |
|-----------|--------|----------------------------|
| match_type | string | body_json_pattern                     |
| data_path          | string | json key路径                  |
| pattern| string | 值，支持正则，优先匹配完整字符串 |

案例：

请求body

```json
{
  "a": {
    "b": 1
  }
}
```

想要匹配b=1

```json
{
  "match_type": "body_json_pattern",
  "data_path": "a.b",
  "pattern": "1"
}
```